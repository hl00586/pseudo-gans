# import os
# os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"
from actions.train import Train_Gans
import argparse
import sys
import numpy as np


def get_args(argv):
    def parser_bool(data):
        if data == 'True':
            return True
        elif data == 'False':
            return False
        else:
            return data

    def dataset_information(args):
        if args.dataset == 'mnist':
            setattr(args, 'img_shape', (28, 28, 1))
            setattr(args, 'output_dim', 10)
        return args

    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', type=parser_bool, default=True, help='verbose during training')
    parser.add_argument('--conv', type=parser_bool, default=True, help='deeper model')
    parser.add_argument('--fake_labels', type=parser_bool, default=True, help='train the discriminator with fake labels')
    parser.add_argument('--epochs', type=int, default=20000, help='Training epochs')
    parser.add_argument('--d_lr', type=float, default=0.0002, help='Discriminator learning rate')
    parser.add_argument('--d_decay', type=float, default=0.5, help='Discriminator Adam Decay')
    parser.add_argument('--g_lr', type=float, default=0.0002, help='Generator learning rate')
    parser.add_argument('--g_decay', type=float, default=0.5, help='Generator Adam Decay')
    parser.add_argument('--split_rate', type=float, default=0.01, help='rate of training samples')
    parser.add_argument('--batch_size', type=int, default=128, help='batch size')
    parser.add_argument('--dataset', type=str, default='mnist', help='dataset name')
    parser.add_argument('--save_to_file', type=str, default='mnist.csv', help='file to save the results')
    args = parser.parse_args(argv)
    args = dataset_information(args)
    return args


def run(args, plot_idx):
    for key, value in vars(args).items():
        print(key, '=', value)
    model = Train_Gans(args)
    gen_acc = model.train()
    model.plot_history(plot_idx)
    base_acc = model.train_baseline(epochs=20)
    history = list(vars(args).values())
    history.insert(0, base_acc)
    history.insert(0, gen_acc)
    if args.save_to_file is not None:
        import csv
        import os
        header = None
        if not os.path.exists('./results/' + args.save_to_file):
            header = list(vars(args).keys())
            header.insert(0, 'Baseline Accuracy')
            header.insert(0, 'Generator Accuracy')
        with open('./results/' + args.save_to_file, 'a') as f:
            writer = csv.writer(f)
            if header is not None:
                writer.writerow(header)
            writer.writerow(history)

    return model


def get_combinations():
    import itertools
    params = {
        'split_rate': [0.0005, 0.001],
        'd_lr': [0.0002, 0.002],
        'd_decay': [0.5, 0.9],
        'g_lr': [0.0002, 0.002],
        'g_decay': [0.5, 0.9],
        'fake_labels': [True, False],
    }
    flat = [[(k, v) for v in vs] for k, vs in params.items()]
    return [dict(items) for items in itertools.product(*flat)], params


def run_combinations(args):
    combinations, _ = get_combinations()
    for c_idx, c in enumerate(combinations):
        for key, value in c.items():
            setattr(args, key, value)
        print('Test model %d/%d'.center(100, '#') % (c_idx + 1, len(combinations)))
        run(args, c_idx)
        print('#' * 100)


if __name__ == '__main__':
    args = get_args(sys.argv[1:])
    run_combinations(args)
