import keras.datasets as Datasets
from keras.utils import to_categorical
import numpy as np


class Dataloader:
    def __init__(self, args):
        self.args = args

    def load_data(self):
        (X_train, y_train), (X_test, y_test) = getattr(Datasets, self.args.dataset).load_data()
        X_train, y_train = self.pre_process(X_train, y_train)
        X_test, y_test = self.pre_process(X_test, y_test)
        X_train, y_train, X_valid, y_valid = self.split_data(X_train, y_train)
        print('#' * 100)
        print('Labelled set:', X_train.shape[0])
        print('Unlabelled set:', X_valid.shape[0])
        print('Test set:', X_test.shape[0])
        print('#' * 100)
        return (X_train, y_train), (X_valid, y_valid), (X_test, y_test)

    def split_data(self, X, y):
        idx = np.random.choice(np.arange(X.shape[0]), size=int(X.shape[0] * self.args.split_rate), replace=False)
        train_idx = np.zeros(X.shape[0]).astype(np.bool)
        train_idx[idx] = True
        valid_idx = np.invert(train_idx)
        return X[train_idx], y[train_idx], X[valid_idx], y[valid_idx]

    def pre_process(self, X, y):
        X = X.astype(np.float32) / 255.
        X = X.reshape(X.shape[0], 28, 28, 1) if self.args.conv else X.reshape(X.shape[0], -1)
        y = to_categorical(y, self.args.output_dim, dtype='float')
        return X, y


