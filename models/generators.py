import keras
from keras.layers import Input, Dense, Dropout, LeakyReLU, Lambda, add, multiply, Flatten, Embedding, Conv2D, \
    MaxPooling2D
from keras.models import Sequential, Model


def build_generator(args, beta=1.):
    if args.conv:
        return convolutional_networks(args, beta)
    return fully_connected(args, beta)


def softargmax(x, beta):
    return keras.activations.softmax(x * beta)


def fully_connected(args, beta):
    model = Sequential()
    model.add(Dense(512))
    model.add(LeakyReLU(alpha=0.2))
    # model.add(Dropout(0.4))
    model.add(Dense(512))
    model.add(LeakyReLU(alpha=0.2))
    # model.add(Dropout(0.4))
    model.add(Dense(args.output_dim))
    model.add(Lambda(lambda x: softargmax(x, beta)))
    return model


def convolutional_networks(args, beta):
    inputs = Input(shape=(28, 28, 1)) if args.dataset == 'mnist' else Input(shape=(32, 32, 3))
    archi = Conv2D(32, (3, 3), padding='same', activation='relu')(inputs)
    archi = MaxPooling2D(pool_size=(2, 2))(archi)
    archi = Dropout(0.5)(archi)
    archi = Conv2D(32, (3, 3), padding='same', activation='relu')(archi)
    archi = MaxPooling2D(pool_size=(2, 2))(archi)
    archi = Dropout(0.5)(archi)
    archi = Flatten()(archi)
    # model_input = multiply([inputs[i], label_embedding])
    archi = Dense(400, activation='relu')(archi)
    archi = Dropout(0.5)(archi)
    archi = Dense(400, activation='relu')(archi)
    archi = Dropout(0.5)(archi)
    # inputs.append(label)
    clf = Dense(args.output_dim)(archi)
    clf = Lambda(lambda x: softargmax(x, beta))(clf)
    model = Model(inputs=inputs, outputs=clf)
    return model