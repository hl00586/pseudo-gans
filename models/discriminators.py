from keras.layers import Input, Dense, Dropout, LeakyReLU, Lambda, add, multiply, Flatten, Embedding, Conv2D, \
    MaxPooling2D, Reshape
from keras.models import Model, Sequential
import numpy as np
import keras.backend as K
import tensorflow as tf
from models.layers import Probability_CLF_Mul


def build_discriminator(args):
    if args.conv:
        return convolutional_networks(args)
    return fully_connected(args)


def sin_cos_embed(y, args):
    y_range = tf.range(y.shape.as_list()[-1], dtype=y.dtype) + 1
    return tf.expand_dims(
        tf.math.sin(tf.reduce_max((y + 1) * y_range, axis=-1) * np.pi / (2 * args.output_dim)),
        axis=-1)


def linear_embed(y, args):
    w, b = 2., 1.
    y_range = tf.range(y.shape.as_list()[-1], dtype=y.dtype)
    return tf.expand_dims(
        tf.reduce_max(y * y_range, axis=-1) * w / args.output_dim + b,
        axis=-1)


def embed_to_channel(inputs, args):
    data, y = inputs
    # y_range = tf.range(y.shape.as_list()[-1], dtype=y.dtype)  # / args.output_dim
    # label = tf.reduce_max(y * y_range, axis=-1, keep_dims=True)
    data_shape = K.shape(data)
    additional = K.ones([data_shape[0], data_shape[1], data_shape[2], args.output_dim]) * \
                 K.reshape(y, [data_shape[0], 1, 1, args.output_dim])
    return tf.concat([data, additional], axis=-1)


def fully_connected_multi_head(args):
    img = Input(shape=(np.prod(args.img_shape),))
    label = Input(shape=(args.output_dim,), dtype='float')

    model = Dense(512)(img)
    model = LeakyReLU(alpha=0.2)(model)
    model = Dense(512)(model)
    model = LeakyReLU(alpha=0.2)(model)
    cluster_output = Dense(1, activation='sigmoid')(model)
    clf_output = Dense(args.output_dim, activation='softmax')(model)
    cluster_model = Model(img, cluster_output)
    clf_model = Model(img, clf_output)
    # label_embedding = Flatten()(Embedding(args.output_dim, np.prod(args.img_shape), input_length=1)(label))
    label_embedding = Lambda(lambda x: sin_cos_embed(x, args))(label)
    # label_embedding = Dense(784)(label)
    model_input = add([img, label_embedding])
    valid = cluster_model(model_input)
    clf = clf_model(model_input)

    return Model([img, label], valid), Model([img, label], clf)


def fully_connected(args):
    model = Sequential()
    model.add(Dense(512))
    model.add(LeakyReLU(alpha=0.2))
    # model.add(Dropout(0.4))
    model.add(Dense(512))
    model.add(LeakyReLU(alpha=0.2))
    # model.add(Dropout(0.4))
    model.add(Probability_CLF_Mul(1, num_centers=args.output_dim))

    img = Input(shape=(np.prod(args.img_shape),))
    label = Input(shape=(args.output_dim,), dtype='float')

    # label_embedding = Flatten()(Embedding(args.output_dim, np.prod(args.img_shape), input_length=1)(label))
    label_embedding = Lambda(lambda x: sin_cos_embed(x, args))(label)
    # label_embedding = Dense(784)(label)
    model_input = add([img, label_embedding])
    valid = model(model_input)

    return Model([img, label], valid)


def convolutional_networks(args):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))
    model.add(Flatten())
    # model_input = multiply([inputs[i], label_embedding])
    model.add(Dense(400, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(400, activation='relu'))
    # inputs.append(label)
    model.add(Dense(1, activation='sigmoid'))

    img = Input(shape=args.img_shape)
    label = Input(shape=(args.output_dim,), dtype='float')
    # label_embedding = Dense(784)(label)
    # label_embedding = Reshape([28, 28, 1])(label_embedding)
    # label_embedding = Lambda(lambda x: sin_cos_embed(x, args))(label)
    # model_input = multiply([img, label_embedding])
    model_input = Lambda(lambda x: embed_to_channel(x, args))([img, label])
    valid = model(model_input)
    return Model([img, label], valid)
