from models.discriminators import build_discriminator
from models.generators import build_generator
from keras.optimizers import Adam
from keras.models import Model
from data_utils.dataloader import Dataloader
import numpy as np
from keras.layers import Input
from tqdm import tqdm


def fake_labels(labels):
    fake_idx = np.random.choice(np.arange(10), size=labels.shape[0])
    fake = np.zeros_like(labels)
    fake[np.arange(fake.shape[0]), fake_idx] = 1
    same_idx = np.argmax(fake, axis=1) == np.argmax(labels, axis=1)
    return fake, np.invert(same_idx)


class Train_Gans:
    def __init__(self, args):
        self.args = args
        D_optimizer = Adam(args.d_lr, args.d_decay)
        G_optimizer = Adam(args.g_lr, args.g_decay)

        # Baseline model
        self.baseline_model = build_generator(args, beta=1)
        self.baseline_model.compile(loss='categorical_crossentropy',
                                    optimizer='adam',
                                    metrics=['accuracy'])

        # Build and compile the discriminator
        self.discriminator = build_discriminator(args)
        self.discriminator.compile(loss='binary_crossentropy',
                                   optimizer=D_optimizer,
                                   metrics=['accuracy'])
        # Build the generator
        self.generator = build_generator(args)
        input_data = Input(shape=args.img_shape) if args.conv else Input(shape=(np.prod(args.img_shape),))
        fake_labels = self.generator(input_data)
        self.discriminator.trainable = False
        valid = self.discriminator([input_data, fake_labels])
        self.combined = Model(input_data, valid)
        self.combined.compile(loss=['binary_crossentropy'],
                              optimizer=G_optimizer, metrics=['accuracy'])
        self.history = {'Loss': {'Discriminator': [], 'Generator': []},
                        'Accuracy': {'Discriminator': [], 'Generator': []}
                        }

    def train(self):
        (X_train, y_train), (X_valid, y_valid), (X_test, y_test) = Dataloader(self.args).load_data()
        batch_size = self.args.batch_size
        valid = np.ones((batch_size, 1))
        fake = np.zeros((batch_size, 1))
        bar = tqdm(range(self.args.epochs), ascii=True, desc='Training start')
        interval = 1
        for epoch in range(self.args.epochs):
            if epoch > 1000:
                interval = 100
            idx = np.random.randint(0, X_train.shape[0], batch_size)
            train_imgs, train_labels = X_train[idx], y_train[idx]
            idx = np.random.randint(0, X_valid.shape[0], batch_size)
            valid_imgs, valid_labels = X_valid[idx], y_valid[idx]
            if epoch % interval == 0:
                gen_labels = self.generator.predict(valid_imgs)
                d_loss_real = self.discriminator.train_on_batch([train_imgs, train_labels], valid)

                if self.args.fake_labels:
                    fake_y, fake_idx = fake_labels(train_labels)
                    d_loss_fake = self.discriminator.train_on_batch([train_imgs[fake_idx],
                                                                     fake_y[fake_idx]], fake[fake_idx])

                d_loss_gen = self.discriminator.train_on_batch([valid_imgs, gen_labels], fake)
                d_loss = 0.5 * np.add(d_loss_real, d_loss_gen)
            g_loss = self.combined.train_on_batch(valid_imgs, valid)
            desc = "Epoch %d/%d [D loss: %f, acc.: %.2f%%] [G loss: %f, acc.: %.2f%%]" \
                   % (epoch, self.args.epochs, d_loss[0], 100 * d_loss[1], g_loss[0], 100 * g_loss[1])
            self.history['Loss']['Discriminator'].append(d_loss[0])
            self.history['Accuracy']['Discriminator'].append(d_loss[1])
            self.history['Loss']['Generator'].append(g_loss[0])
            self.history['Accuracy']['Generator'].append(g_loss[1])
            if self.args.verbose:
                bar.set_description(desc=desc, refresh=True)
        res = self.generator.predict(X_test)
        acc = np.sum(np.argmax(res, axis=1) == np.argmax(y_test, axis=1)) / y_test.shape[0]
        print(' ')
        print('Generator accuracy on the test set', acc)
        return acc

    def train_baseline(self, epochs=None):
        (X_train, y_train), (X_valid, y_valid), (X_test, y_test) = Dataloader(self.args).load_data()
        epochs = self.args.epochs if epochs is None else epochs
        self.baseline_model.fit(X_train, y_train, epochs=epochs, verbose=False)
        print('Baseline model', self.baseline_model.evaluate(X_test, y_test, verbose=0))
        return self.baseline_model.evaluate(X_test, y_test, verbose=0)[1]

    def plot_history(self, plot_idx):
        import matplotlib.pyplot as plt
        import seaborn as sns
        sns.set()
        for key in self.history:
            plt.clf()
            plt.title(key)
            for m_type in self.history[key]:
                plt.plot(self.history[key][m_type], label=m_type)
            plt.legend()
            plt.savefig('./results/acc_loss/{}_{}.png'.format(plot_idx, key))

    def visualise_inputs(self, X, y):
        hidden_model = Model(self.discriminator.inputs, self.discriminator.layers[3].output)
        return hidden_model.predict([X, y])
